#include <Arduino.h>
#include <SoftwareSerial.h>

int dif;
int sensors[3];
int greens[3];

SoftwareSerial nano(2, 3);

void mov(int v1, int v2, int v3) { //speed -256 az +255
    int v[3] = {v1+256, v2+256, v3+256};
    for (int i = 0; i < 3; i++) {
        nano.write(int(v[i] / 256));
        nano.write(v[i] % 256);
    }
}

int calibration(int pin) {
    int green = 0;
    for (int i = 0; i < 10; i++) {
        green += analogRead(sensors[pin]);
    }
    green /= 10;
    return green;
}

bool isWhite(int pin) {
    return analogRead(sensors[pin]) > greens[pin]+dif;
}

bool motorsBegin() {
    nano.begin(9600);
    int time = millis();
    while(!nano.available()) {
        if (millis()-time >= 10000) {
            return false;
        }
    }
    return true;
}

void sensorsBegin(int pin1, int pin2, int pin3, int difI) {
    dif = difI;

    sensors[0] = pin1;
    sensors[1] = pin2;
    sensors[2] = pin3;

    greens[0] = calibration(0);
    greens[1] = calibration(1);
    greens[2] = calibration(2);
}
