//Program for controlling motors (bottom Arduino)
//Board: Arduino Nano
//Processor: ATmega328P (Old Bootloader)

int motors_pins[3][3] = {{2, 3, 10}, {4, 5, 11}, {7, 8, 9}};

void setMotor(int motor, int mSpeed) {
    if (mSpeed > 0) {
        digitalWrite(motors_pins[motor][0], LOW);
        digitalWrite(motors_pins[motor][1], HIGH);
    } else if (mSpeed < 0) {
        digitalWrite(motors_pins[motor][0], HIGH);
        digitalWrite(motors_pins[motor][1], LOW);
    } else {
        digitalWrite(motors_pins[motor][0], LOW);
        digitalWrite(motors_pins[motor][1], LOW);
    }
    analogWrite(motors_pins[motor][2], abs(mSpeed));
}

void setup() {
    Serial.begin(9600);
    Serial.write(1);
}

void loop() {
    if (Serial.available() >= 6) {
        setMotor(0, Serial.read()*256+Serial.read()-256);
        setMotor(1, Serial.read()*256+Serial.read()-256);
        setMotor(2, Serial.read()*256+Serial.read()-256);
    }
}
