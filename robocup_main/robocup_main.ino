//Main program (top Arduino)
//Board: Arduino/Genuino Uno

#include <SPI.h>
#include <Pixy.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include "motors_sensors.h"

#define BALL_RANGE   70   //size of middle section of view for ball [px]
#define GOAL_RANGE   120  //size of middle section of view for goal [px]
#define COMP_RANGE   20   //size of middle section of compass [deg]
#define SPEED        150  //speed (0-255)
#define FULL_SPEED   220  //full speed (0-255)
#define TURN_SPEED   55   //turning speed (0-255)
#define SIDE_CONST   2.0  //multiplier for back motor when moving left or right
#define BALL_SIGN    1    //ball signature (1-7)
#define B_GOAL_SIGN  2    //blue goal signature (1-7)
#define Y_GOAL_SIGN  3    //yellow goal signature (1-7)
#define SKIP_FRAMES  2    //step of checking relevant frames
#define MOV_TIME     60   //time between moves to ball [ms]
#define LINE_TIME    50   //time between moves from line [ms]
#define LINE_DIF     120  //light difference between ground and line (0-1024) ...120
#define M_LINE_DELAY 1000 //max time of moving from line [ms]

bool lineLeft = false, lineRight = false, lineFront = false;
bool isBall = false, isGoal = false;
bool inRange = false;
bool enabled = false;
int goal_sign = B_GOAL_SIGN;
int numblocks = 0;
float heading = 0;
int blocks_iter = 0;
long time_of_move = 0, time_of_line = 0;
int cur_speed = 0;

Adafruit_SSD1306 display(128, 32, &Wire);
Adafruit_BNO055 bno = Adafruit_BNO055(55, 0x28);
Pixy pixy;
Block ball, goal;

void setSpeed(int v, float r1, float r2, float r3) {
    cur_speed = v;
    mov(r1*v, r2*v, r3*v);
}

void diaRight(int v=SPEED)       { setSpeed(v, 0, -1, 1); }
void diaLeft(int v=SPEED)        { setSpeed(v, 1, 0, -1); }
void right(int v=SPEED)          { setSpeed(v, -1/SIDE_CONST, -1/SIDE_CONST, 1); }
void left(int v=SPEED)           { setSpeed(v, 1/SIDE_CONST, 1/SIDE_CONST, -1); }
void turnRight(int v=TURN_SPEED) { setSpeed(v, -1, -1, -1); }
void turnLeft(int v=TURN_SPEED)  { setSpeed(v, 1, 1, 1); }
void forward(int v=SPEED)        { setSpeed(v, 1, -1, 0); }
void backward(int v=SPEED)       { setSpeed(v, -1, 1, 0); }
void motorsStop()                { setSpeed(0, 0, 0, 0); }

void printDisplay(String text) {
    display.setTextSize(1);
    display.setCursor(0, 0);
    display.clearDisplay();
    display.print(text);
    display.display();
}

void showXOnDisplay(int xL, int xB, float hf) {
    String sLabels[3] = {"F", "L", "R"};
    String cLabels[3] = {"B", "G", "C"};
    int h = int(hf);
    int xH = map((h+180)%360, 0, 360, 0, 319);
    int sensors[3] = {analogRead(A0), analogRead(A1), analogRead(A2)};
    int coords[3] = {isBall ? xL : -1, isGoal ? xB : -1, xH};
    display.clearDisplay();
    display.setTextSize(1);
    display.drawLine(26, 0, 26, display.height()-5, SSD1306_WHITE);
    for (int i=0; i<3; i++) {
        display.setCursor(0, display.height()/3*i);
        display.print(sLabels[i]);
        display.print(sensors[i]);
        display.setCursor(30, display.height()/3*i);
        display.print(cLabels[i]);
        display.print((i == 2) ? h : coords[i]);
        if (coords[i] != -1) {
            display.fillCircle(
                map(coords[i], 0, 319, 65, display.width()),  //x
                display.height()/3*i+2,                       //y
                2,                                            //radius
                SSD1306_WHITE                                 //color
            );
        }
    }
    display.display();
}

bool getBlock(int sig, Block *block) {
    for (int b=0; b<numblocks; b++) {
        if (pixy.blocks[b].signature == sig) {
            *block = pixy.blocks[b];
            return true;
        }
    }
    return false;
}

int getGoal() {
    if (getBlock(Y_GOAL_SIGN, &goal)) {
        return Y_GOAL_SIGN;
    }
    return B_GOAL_SIGN;
}

float readCompass() {
    sensors_event_t orientation;
    bno.getEvent(&orientation, Adafruit_BNO055::VECTOR_EULER);
    return orientation.orientation.x;
}

void setup() {
    pinMode(7, INPUT);
    Serial.begin(9600);
    sensorsBegin(A0, A1, A2, LINE_DIF);
    Serial.println(motorsBegin());
    pixy.init();
    bno.begin(bno.OPERATION_MODE_IMUPLUS);
    display.begin(SSD1306_SWITCHCAPVCC, 0x3C);

    display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(SSD1306_WHITE);
    display.setCursor(18, 12);
    display.print("Griffins");
    display.display();
    long start_time = millis();
    while (numblocks == 0 && millis()-start_time < 10000) {
        numblocks = pixy.getBlocks();
    }
    goal_sign = getGoal();
    printDisplay("Goal sign: " + String(goal_sign));
}

void loop() {
    if (digitalRead(7)) {
        enabled = !enabled;
        while (digitalRead(7)) {}
    }
    if (enabled) {
        //--------------- line avoiding ----------------
        lineFront = lineFront || isWhite(0);
        lineLeft  = lineLeft  || isWhite(1);
        lineRight = lineRight || isWhite(2);

        if ((lineFront || lineLeft || lineRight) && time_of_line == 0) {
            time_of_line = millis();
        }

        if (time_of_line > 0 && millis()-time_of_line >= LINE_TIME) {
            int line_delay = sq(cur_speed/255.0) * M_LINE_DELAY;
            if (lineFront) {
                backward(FULL_SPEED);
            } else if (lineLeft && lineRight) {
                forward(FULL_SPEED);
            } else if (lineLeft) {
                right(FULL_SPEED);
            } else if (lineRight) {
                left(FULL_SPEED);
            }
            tone(10, 800, line_delay);
            delay(line_delay);
            lineFront = lineLeft = lineRight = false;
            time_of_line = 0;
        }

        numblocks = pixy.getBlocks();
        if (numblocks) {
            blocks_iter++;
        }

        if (millis()-time_of_move >= MOV_TIME || (numblocks && blocks_iter % SKIP_FRAMES == 0)) {
            time_of_move = millis();
            heading = readCompass();
            isBall = getBlock(BALL_SIGN, &ball);
            isGoal = getBlock(goal_sign, &goal);

            //--------------- turning to goal ---------------
            inRange = false;
            if (isGoal) {
                if (goal.x < 320/2-GOAL_RANGE/2) { //the goal is on the left
                    turnLeft();
                } else if (goal.x > 320/2+GOAL_RANGE/2) { //the goal is on the right
                    turnRight();
                } else { //turned correct
                    inRange = true;
                }
            } else { //does not see goal, so starts using compass
                if (heading > COMP_RANGE/2 && heading <= 180) { //the goal is on the left
                    turnLeft();
                } else if (heading < 360-COMP_RANGE/2 && heading > 180) { //the goal is on the right
                    turnRight();
                } else { //turned correct
                    inRange = true;
                }
            }

            //--------------- moving to ball ----------------
            if (inRange) { //turned correct
                if (numblocks) { //sees something
                    if (isBall) {
                        if (ball.x < 320/2-BALL_RANGE/2) { //the ball is on the left
                            diaLeft();
                        } else if (ball.x > 320/2+BALL_RANGE/2) { //the ball is on the right
                            diaRight();
                        } else { //ball is in the middle
                            forward(FULL_SPEED);
                        }
                    } else { //does not see ball
                        backward();
                    }
                } else { //does not see anything
                    backward();
                }
            }
            showXOnDisplay(ball.x, goal.x, heading);
        }
    } else { //moving disabled
        motorsStop();
    }
}
