# RoboCup 2022, team _Griffins_

- [Poster](https://drive.google.com/file/d/10n_-w3nhX5PRl2p-SWuz6KwyWvckR_6i/view?usp=sharing)
- [Thingiverse](https://www.thingiverse.com/matusmisiak/collections/robocup)

## Hardware

### Parts

- 1x Arduino Uno
- 1x Arduino Nano
- 3x Pololu-3475 31:1 12V brushless DC motor
- 3x Nexus Robot 58mm Omni Wheel
- 2x L298N dual full-bridge driver
- 1x Adafruit BNO055 Absolute Orientation Sensor
- 1x Pixy CMUcam5 Image Sensor
- 1x Adafruit FeatherWing OLED 128x32
- 1x Tattu 2300mAh 11.1V LiPo Battery
- \+ white LEDs, LDRs, switches, connectors...

### 3D models

- Designed in Fusion 360
- STL files can be found in [`hardware/stl/`](/hardware/stl/)

### Scheme

![Connection scheme](/hardware/robocup2020_bb2.png)

## Software

There is a separate Arduino for motors, due to lack of pins, clarity and possibility of multiprocessing. Its program is located in [`robocup_motor_control/`](/robocup_motor_control/)

The main program, located in [`robocup_main/`](/robocup_main/) is running on Arduino Uno. It is communicating with Arduino Nano via UART, with the use of our custom library [`motors_sensors`](/motors_sensors/).

> Note: You have to copy [`motors_sensors/`](/motors_sensors/) folder to `libraries` in your Arduino sketch folder before compilation.

## Photos

![Front](/img/photos/front.jpg)
![Rear](/img/photos/rear.jpg)
![Side](/img/photos/side.jpg)
![Top](/img/photos/top.jpg)
![Bottom](/img/photos/bottom.jpg)
![Render](/img/render-v25.png)
