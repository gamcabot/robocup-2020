#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
//#include "gamcabot.c"

//static const unsigned char PROGMEM logo_bmp[32];

Adafruit_SSD1306 display(128, 32, &Wire);

void setup() {
    pinMode(10, INPUT);
    display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
}

void loop() {
//    display.clearDisplay();
//    display.drawBitmap(0, 0, gamcabot, 128, 32, 1);
//    display.display();
//
//    delay(2000);
    if (digitalRead(10)) {
        display.clearDisplay();
        display.setTextSize(2);
        display.setTextColor(SSD1306_WHITE);
        display.setCursor(0, 0);
        display.print(millis());
        display.println("ms");
        display.display();
        delay(10);
    }
}
