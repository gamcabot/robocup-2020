#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>

double heading = 0;
double DEG_2_RAD = 0.01745329251;
int radius = 15;

Adafruit_SSD1306 display(128, 32, &Wire);
Adafruit_BNO055 bno = Adafruit_BNO055(55, 0x28);

void setup() {
//    Serial.begin(9600);
    display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
    bno.begin();
}

void loop() {
    sensors_event_t orientation;
    bno.getEvent(&orientation, Adafruit_BNO055::VECTOR_EULER);
    heading = orientation.orientation.x*DEG_2_RAD;
    display.clearDisplay();
    display.fillCircle(display.width()/2, display.height()/2, 2, SSD1306_WHITE);
    display.drawCircle(display.width()/2, display.height()/2, radius, SSD1306_WHITE);
    display.drawLine(
        display.width()/2,
        display.height()/2,
        (radius-2)*sin(heading)+display.width()/2,
        (radius-2)*cos(heading)+display.height()/2,
        SSD1306_WHITE
    );
    display.println(String(heading));
    display.display();
//    Serial.println(heading);
    delay(10);
}
