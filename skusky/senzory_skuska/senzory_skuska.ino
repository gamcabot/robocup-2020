#include "motors_sensors.h"

int r = 190;

void setup() {
    sensorsBegin(A0, A1, A2);
    motorsBegin();

    mov(r, -r, 0);
    while(!isWhite(0));
    mov(-r, r, 0);
    tone(10, 1000, 250);
    delay(500);
    mov(0, 0, 0);

    delay(5000);

    mov(0, r, -r);
    while(!isWhite(1));
    mov(0, -r, r);
    tone(10, 1000, 250);
    delay(500);
    mov(0, 0, 0);

    delay(5000);

    mov(-r, 0, r);
    while(!isWhite(2));
    mov(r, 0, -r);
    tone(10, 1000, 250);
    delay(500);
    mov(0, 0, 0);

    delay(5000);
}

void loop() {

}
