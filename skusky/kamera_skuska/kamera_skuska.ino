#include <SPI.h>  
#include <Pixy.h>
#include "motory_senzory.h"

int rozsah = 90;

Pixy pixy;

void setup() {
    pixy.init();
    motoryBegin();
}

void loop() {
    uint16_t blocks;
    blocks = pixy.getBlocks();
    if (blocks) {
        if (pixy.blocks[0].x < 320/2-rozsah/2) { //left
            mov(70, 70, -98);
        } else if (pixy.blocks[0].x > 320/2+rozsah/2) { //right
            mov(-70, -70, 98);
        } else { //forward
            mov(190, -190, 0);
        }
    } else {
        mov(0, 0, 0);
    }
    delay(50);
}
