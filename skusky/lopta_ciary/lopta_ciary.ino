#include <SPI.h>  
#include <Pixy.h>
#include "motory_senzory.h"

int rozsah = 110;
int r = 150;
float RM = 2.0;
int del = 200;

bool cVlavo = false;
bool cVpravo = false;
int i = 0;


Pixy pixy;

void setup() {
    pixy.init();
    senzoryBegin(A0, A1, A2);
    motoryBegin();
}

void loop() {
    if (jeBiela(1)) {
        cVlavo = true;
    }
    if (jeBiela(2)) {
        cVpravo = true;
    }
    
    if (millis()%20 == 0) {
        if (pixy.getBlocks()) { //lopta je
            if (pixy.blocks[0].x < 320/2-rozsah/2) { //lopta je vlavo
                if (cVlavo) { //vlavo ma ciaru
                    mov(-r, -r, r*RM); //ide vpravo
                    tone(10, 800, del);
                    cVlavo = false;
                    delay(del);
                } else { //vlavo nema ciaru
                    mov(r, r, -r*RM); //ide vlavo
                }
            } else if (pixy.blocks[0].x > 320/2+rozsah/2) { //lopta je vpravo
                if (cVpravo) { //vpravo ma ciaru
                    mov(r, r, -r*RM); //ide vlavo
                    tone(10, 800, del);
                    cVpravo = false;
                    delay(del);
                } else { //vpravo nema ciaru
                    mov(-r, -r, r*RM); //ide vpravo
                }
            } else { //lopta je v strede
                mov(0, 0, 0);
            }
        } else { //lopta neni
            mov(0, 0, 0);
        }
//        if (!jeBiela(1))
//            cVlavo = false;
//        if (!jeBiela(2))
//            cVpravo = false;
    }
    i++;
}
